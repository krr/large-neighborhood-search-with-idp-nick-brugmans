Name: Maximal Clique problem
Complexity: NP-hard
Instances: https://www.mat.unical.it/aspcomp2013/MaximalClique