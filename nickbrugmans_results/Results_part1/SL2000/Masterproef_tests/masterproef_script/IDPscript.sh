#!/bin/bash
# memory limit is in MB
tlim=$1
let hardtlim=3+$tlim
let mlim=1024*$2
let hardmlim=$mlim+50000
 
kbs=$3
problem=$4
command=$5
struct=$6
location=$7
res_file="$location.sol"
err_file="$location.err"
echo " "
echo $kbs
echo $problem
echo $command
echo $struct
echo $res_file
 
hash=`echo -n $res_file | md5sum | awk '{ print $1 }'`
 
if [ $tlim -gt 0 ]; then
  ulimit -Ht $hardtlim -St $tlim # NOTE: hard limit apparently needs to be first argument!? -Ht $hardtlim
fi
if [ $mlim -gt 0 ]; then
  ulimit -Hv $hardmlim -Sm $mlim # NOTE: hard limit apparently needs to be first argument!? -Hv $hardmlim
fi
 
# Note the use of the tmp file, to avoid problems with time writing to $res_file
(/usr/bin/time --format="cpu time: %U\nmax memory: %M\n-----OUTPUT-----" --output=$res_file $kbs --nowarnings $problem -e "$command" $struct 2> $err_file)
ret=$?
exit $ret
