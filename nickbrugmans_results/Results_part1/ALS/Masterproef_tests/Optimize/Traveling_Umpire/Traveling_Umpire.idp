/** 
* Travelling Umpire
*/

vocabulary V{
// Objects
  type Umpire 
  type Team 
  type Round isa int
  type Number isa int
  type Match
	
	HomeTeam(Match):Team
	AwayTeam(Match):Team
	GetRound(Match):Round
	Distance(Team,Team):Number
	
	ConsecutiveHomes(Number)
	ConsecutiveTeams(Number)
	
	// Search:
	Assign(Match):Umpire
	SameUmpire(Match,Match)
	SameHomeTeam(Match,Match)
}

theory T:V{     
	// each game has an umpire 
	// in the assign function constraint
	
	//  each umpire crew works exactly one game per slot
	!u r: ?m: Assign(m)=u & GetRound(m)=r.
	!u r m m2: Assign(m)=u & GetRound(m)=r & Assign(m2)=u & GetRound(m2)=r => m=m2.
	!u r: ?1 m: Assign(m)=u & GetRound(m)=r.
	
	//  each umpire crew sees each team at least once at the team’s home
	!t u: ?m: Assign(m)=u & HomeTeam(m)=t.
	
	{
		SameUmpire(m,m2) <- Assign(m)=Assign(m2).
		SameHomeTeam(m,m2) <- HomeTeam(m)=HomeTeam(m2).
	}
	
	// no umpire is in a home site more than once in any k rounds
	!r m m2: ConsecutiveHomes(r) & GetRound(m2)=GetRound(m)+r & SameUmpire(m,m2) => ~SameHomeTeam(m,m2).
	
	// no umpire sees a team more than once in any k consecutive rounds
 	!r m m2: ConsecutiveTeams(r) & GetRound(m2)=GetRound(m)+r & SameUmpire(m,m2) => AwayTeam(m)~=AwayTeam(m2) & AwayTeam(m)~=HomeTeam(m2) & HomeTeam(m)~=AwayTeam(m2).
}

term Obj:V{
  sum{m m2 : GetRound(m2)=GetRound(m)+1 & Assign(m)=Assign(m2) : Distance(HomeTeam(m),HomeTeam(m2))}
}

query ObjValue:V{
  {x:x=sum{m m2 : GetRound(m2)=GetRound(m)+1 & Assign(m)=Assign(m2) : Distance(HomeTeam(m),HomeTeam(m2))} }
}

query Output:V{
  {u r h a: ?m: Assign(m)=u & GetRound(m)=r & HomeTeam(m)=h & AwayTeam(m)=a }
}



procedure getStructure(){
	return S
}

procedure IDPproc(){
	stdoptions.cpsupport=false
	stdoptions.verbosity.solving = 1
	result= minimize(T,S,Obj)[1]
}
procedure LNSproc(alpha , beta, gamma , stagn, runtime){
	idpintern.LNS(T,S,Obj,alpha,beta,gamma, stagn, runtime)
}
procedure LNSCompleteProc(runtime){
	result = idpintern.LNSComplete(T,S,Obj, runtime)
}
procedure LNSCompleteFixedframesize(framesize, runtime){
	result = idpintern.LNSComplete(T,S,Obj,framesize ,runtime)
}
procedure ALSproc(){
	stdoptions.localsearch = "standard"
	stdoptions.verbosity.localsearch = 1
	stdoptions.verbosity.solving = 1
	result= minimize(T,S,Obj)[1]
}

structure Small:V{
Umpire={1..2}
Distance={
1,1,0; 1,2,745; 1,3,665; 1,4,929; 
2,1,745; 2,2,0; 2,3,80; 2,4,337; 
3,1,665; 3,2,80; 3,3,0; 3,4,380; 
4,1,929; 4,2,337; 4,3,380; 4,4,0; 
}
GetRound={1,1; 2,1; 3,2; 4,2; 5,3; 6,3; 7,4; 8,4; 9,5; 10,5; 11,6; 12,6; }
HomeTeam={1,1; 2,2; 3,1; 4,3; 5,1; 6,3; 7,3; 8,4; 9,2; 10,4; 11,2; 12,4; }
AwayTeam={1,3; 2,4; 3,2; 4,4; 5,4; 6,2; 7,1; 8,2; 9,1; 10,3; 11,3; 12,1; }
ConsecutiveHomes={1}
ConsecutiveTeams={}
}
