In this folder you will find all the experiments and result done for my thesis.
For the experiment specifications I refer to my thesis.


The folder binaries and source code contains 4 different programs:

 - IDP folder contains the source code and binaries of normal idp version used to execute IDP and IDPNOCP experiments.

 - ALS-install contains only the binaries to conduct the ALS experiments.

 - LNS fixed SearchLimit contains the source code and binaries that were used for F300,F2000 and fullSearch

 - LNS with reinforcing contains the soruce code and binaries that were used for SL300 and SL2000

The folder Results_part1 contains the results that were discussed in section 7.3.1 and 7.3.2

The folder Results_part2 contains the results that were discussed in section 7.3.3 and 7.3.4

Both contain a merged folder that contains all information extracted from that part of the experiments.

Every configuration(program,parameters) contains an optimize folder with all problems were executed.
The folder of a problem contains a folder instances, results and parsed.

	- instances contains all idp structures that were used for a problem.
	- results contains the raw results
	- parsed contains a csv for every instance with the results.

Finally every problem folder contains an IDP program with a different procedure for every of the 4 programs:

Used to test IDP and IDPNOCP by turning off cp:

procedure IDPproc()

Used for SL300 and SL2000

procedure LNSsearchLimit(alpha , beta, gamma , stagn, runtime, searchLimit)

Used for F300 and F2000

procedure LNSCompleteProc(SL, runtime)

Used for ALS:

procedure ALSproc()