#!/usr/bin/env bash

for file in instances/*
do
  echo $file
  filename=$(basename "$file")
  corename="${filename%.*}"
  corename="${corename%.*}"
  idp traveling_salesman.idp $file -e "printfacts()" --nowarnings > aspinstances/$corename.asp
done
