/*
Traveling Tournament

Double round robin (A at B and B at A): n teams need 2*n-2 slots.
No more than three consecutive home or three consecutive road games for any team
No repeaters (A at B, followed immediately by B at A)
Objective is to minimize distance traveled (assume teams begin in their home city and must return there after the tournament)
City names are in the order ATL NYM PHI MON FLA PIT CIN CHI STL MIL HOU COL SF SD LA ARI where NLx takes the first x cities.
Data files contain the distance matrix (square, symmetric).
*/

vocabulary V {
    type Team
    type Round isa int
    type Dist isa int
    
    PlaysIn(Team,Team):Round
    Location(Round,Team):Team
    Distance(Team,Team):Dist
    Next(Round):Round
		//PlaysHome(Round,Team)
		MatchesEachRound:Round
}

theory T:V{
	{
		Next(MAX[:Round])=MIN[:Round].
		!x: Next(x)=x+1 <- Round(x+1).
	}
	{
		MatchesEachRound=#{t:Team(t)}/2.
	}
	
	// teams start at home positions
  !t1 t2: t1=t2 <=> PlaysIn(t1,t2)=0.
  
  // every round, every team plays against exactly one team
  !r t: ?1 t2: PlaysIn(t,t2)=r | PlaysIn(t2,t)=r.
  // every round has a fixed number of matches
  !r[Round]: r>0 => #{t1 t2 : PlaysIn(t1,t2)=r}=MatchesEachRound.
  
  
  //{ !r t: PlaysHome(r,t) <- ?t2: PlaysIn(t,t2)=r. }
  
  !t r[Round]: r>3 => ?t2: PlaysIn(t,t2)=r | PlaysIn(t,t2)=r-1 | PlaysIn(t,t2)=r-2 | PlaysIn(t,t2)=r-3.
  !t r[Round]: r>3 => ?t2: PlaysIn(t2,t)=r | PlaysIn(t2,t)=r-1 | PlaysIn(t2,t)=r-2 | PlaysIn(t2,t)=r-3.
  //!t r[Round]: r>3 => PlaysHome(r,t) | PlaysHome(r-1,t) | PlaysHome(r-2,t) | PlaysHome(r-3,t).
	//!t r[Round]: r>3 => ~PlaysHome(r,t) | ~PlaysHome(r-1,t) | ~PlaysHome(r-2,t) | ~PlaysHome(r-3,t).
        
//No repeaters (A at B, followed immediately by B at A) 
    !t1 t2: t1~=t2 => PlaysIn(t1,t2)>PlaysIn(t2,t1)+1 | PlaysIn(t2,t1)>PlaysIn(t1,t2)+1.
    
    {
			!r t: Location(r,t)=t <- ?t2: PlaysIn(t,t2)=r.
			!r t t2: Location(r,t)=t2 <- PlaysIn(t2,t)=r.
		}
}

term Obj:V{
	sum{r t : Round(r) & Team(t) : Distance(Location(r,t),Location(Next(r),t))}
}


procedure getStructure(){
	return S
}

procedure IDPproc(){
	stdoptions.cpsupport=false
	stdoptions.verbosity.solving = 1
	result= minimize(T,S,Obj)[1]
}
procedure LNSproc(alpha , beta, gamma , stagn, runtime){
	idpintern.LNS(T,S,Obj,alpha,beta,gamma, stagn, runtime)
}
procedure LNSCompleteProc(runtime){
	result = idpintern.LNSComplete(T,S,Obj, runtime)
}
procedure LNSCompleteFixedframesize(framesize, runtime){
	result = idpintern.LNSComplete(T,S,Obj,framesize ,runtime)
}
procedure LNSsearchLimit(alpha , beta, gamma , stagn, runtime, searchLimit){
	idpintern.LNSsearchLimit(T,S,Obj,alpha,beta,gamma, stagn, runtime, searchLimit)
}
procedure ALSproc(){
	stdoptions.localsearch = "standard"
	stdoptions.verbosity.localsearch = 1
	stdoptions.verbosity.solving = 1
	result= minimize(T,S,Obj)[1]
}

structure NL4:V{
    Round={0..6}
    Team={
        ATL;NYM;PHI;MON;
    }
    Distance={
ATL,ATL->   0; ATL,NYM -> 745; ATL,PHI-> 665; ATL,MON-> 929; 
NYM,ATL-> 745; NYM,NYM ->   0; NYM,PHI->  80; NYM,MON-> 337; 
PHI,ATL-> 665; PHI,NYM ->  80; PHI,PHI->   0; PHI,MON-> 380; 
MON,ATL-> 929; MON,NYM -> 337; MON,PHI-> 380; MON,MON->   0;      
    }
}

structure NL6:V{
Round={0..10}
Distance={
T0,T0,0; T0,T1,745; T0,T2,665; T0,T3,929; T0,T4,605; T0,T5,521; 
T1,T0,745; T1,T1,0; T1,T2,80; T1,T3,337; T1,T4,1090; T1,T5,315; 
T2,T0,665; T2,T1,80; T2,T2,0; T2,T3,380; T2,T4,1020; T2,T5,257; 
T3,T0,929; T3,T1,337; T3,T2,380; T3,T3,0; T3,T4,1380; T3,T5,408; 
T4,T0,605; T4,T1,1090; T4,T2,1020; T4,T3,1380; T4,T4,0; T4,T5,1010; 
T5,T0,521; T5,T1,315; T5,T2,257; T5,T3,408; T5,T4,1010; T5,T5,0; 
}
}
